﻿namespace Protocol.Client.GUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.syncedFolderWatcher = new System.IO.FileSystemWatcher();
            this.directoryView = new System.Windows.Forms.ListView();
            this.nameHeader = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.filePath = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Size = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.checksum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Status = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.consoleBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.synchronise = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.listSync = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.syncedFolderWatcher)).BeginInit();
            this.SuspendLayout();
            // 
            // syncedFolderWatcher
            // 
            this.syncedFolderWatcher.EnableRaisingEvents = true;
            this.syncedFolderWatcher.SynchronizingObject = this;
            // 
            // directoryView
            // 
            this.directoryView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.nameHeader,
            this.filePath,
            this.Size,
            this.checksum,
            this.Status});
            this.directoryView.Location = new System.Drawing.Point(-3, 80);
            this.directoryView.Name = "directoryView";
            this.directoryView.Size = new System.Drawing.Size(766, 548);
            this.directoryView.TabIndex = 0;
            this.directoryView.UseCompatibleStateImageBehavior = false;
            this.directoryView.View = System.Windows.Forms.View.Details;
            this.directoryView.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.directoryView_MouseDoubleClick);
            // 
            // nameHeader
            // 
            this.nameHeader.Text = "FileName";
            this.nameHeader.Width = 125;
            // 
            // filePath
            // 
            this.filePath.Text = "filePath";
            // 
            // Size
            // 
            this.Size.Text = "Size";
            // 
            // checksum
            // 
            this.checksum.Text = "Checksum";
            this.checksum.Width = 97;
            // 
            // Status
            // 
            this.Status.Text = "Status";
            // 
            // consoleBox
            // 
            this.consoleBox.Location = new System.Drawing.Point(781, 80);
            this.consoleBox.Multiline = true;
            this.consoleBox.Name = "consoleBox";
            this.consoleBox.Size = new System.Drawing.Size(339, 548);
            this.consoleBox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(778, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Console:";
            // 
            // synchronise
            // 
            this.synchronise.Location = new System.Drawing.Point(588, 32);
            this.synchronise.Name = "synchronise";
            this.synchronise.Size = new System.Drawing.Size(175, 42);
            this.synchronise.TabIndex = 3;
            this.synchronise.Text = "Synchroniseren";
            this.synchronise.UseVisualStyleBackColor = true;
            this.synchronise.Click += new System.EventHandler(this.synchronise_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(272, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Bestandslijst(dubbel klikken op een rij voor verwijdering):";
            // 
            // listSync
            // 
            this.listSync.Location = new System.Drawing.Point(467, 32);
            this.listSync.Name = "listSync";
            this.listSync.Size = new System.Drawing.Size(115, 42);
            this.listSync.TabIndex = 6;
            this.listSync.Text = "Status bijwerken";
            this.listSync.UseVisualStyleBackColor = true;
            this.listSync.Click += new System.EventHandler(this.listSync_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1132, 640);
            this.Controls.Add(this.listSync);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.synchronise);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.consoleBox);
            this.Controls.Add(this.directoryView);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.syncedFolderWatcher)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.IO.FileSystemWatcher syncedFolderWatcher;
        private System.Windows.Forms.ListView directoryView;
        private System.Windows.Forms.ColumnHeader nameHeader;
        private System.Windows.Forms.ColumnHeader filePath;
        private System.Windows.Forms.ColumnHeader Size;
        private System.Windows.Forms.ColumnHeader checksum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox consoleBox;
        private System.Windows.Forms.Button synchronise;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ColumnHeader Status;
        private System.Windows.Forms.Button listSync;
    }
}

