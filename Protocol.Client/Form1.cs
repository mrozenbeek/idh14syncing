﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Protocol.Client.GUI.Providers;
using Protocol.Core.Client;
using Protocol.Core.Client.Request;
using Protocol.Core.Client.Response;
using Protocol.Core.Enums;
using Protocol.Core.Helpers;

namespace Protocol.Client.GUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            if (Directory.Exists(FolderProvider.ChecksumFolder) == false)
                Directory.CreateDirectory(FolderProvider.ChecksumFolder);

            GetSyncStatus();

            syncedFolderWatcher = new FileSystemWatcher();
            syncedFolderWatcher.Path = FolderProvider.ClientFolder;
            syncedFolderWatcher.NotifyFilter = NotifyFilters.LastAccess
               | NotifyFilters.LastWrite
               | NotifyFilters.FileName
               | NotifyFilters.DirectoryName;
            syncedFolderWatcher.Created += OnCreate;
            syncedFolderWatcher.EnableRaisingEvents = true;

            consoleBox.AppendText($"{Environment.NewLine} Watching file direcorty. All changes will be shown here");
        }


        void OnCreate(object sender, FileSystemEventArgs e)
        {
            string log = string.Format("{0} | Renamed from {1}",
                                       e.FullPath, e.Name);


            AddFile(e.Name);
            directoryView.Invoke(new Action(() =>
            {
                ListViewItem item = new ListViewItem(new[] { e.Name, e.FullPath + "/" + e.Name,"size",ChecksumHelper.CreateChecksum(File.ReadAllBytes(e.FullPath))});
                item.Name = e.Name;
                directoryView.Items.Add(item);
            }));

        }

        private  void synchronise_Click(object sender, EventArgs e)
        {
         Sync();
            
        }


        private void Sync()
        {
            syncedFolderWatcher.EnableRaisingEvents = false;

            SyncClient client = new SyncClient(
            IPAddress.Parse(FolderProvider.Host), FolderProvider.port);

            Encoding coder = Encoding.UTF8;

            ListResponseEntity response = client.SendCommand<ListResponseEntity>(new ListRequestentity());

            consoleBox.AppendText($"{Environment.NewLine}Sending LIST command to server.{Environment.NewLine}");

            consoleBox.AppendText($"{Environment.NewLine}Found {response.files.Count} to sync." + Environment.NewLine);


            DirectoryInfo dinfo = new DirectoryInfo(FolderProvider.ClientFolder);
            FileInfo[] files = dinfo.GetFiles("*");

            //Loop through server files and local files to determin what and how to sync.
            foreach (FileEntityFileObject file in response.files)
            {
                string filename = FolderProvider.ClientFolder +  coder.GetString(Convert.FromBase64String(file.filename));

                if (files.Any(el=> el.Name == filename))
                {
                    if (file.checksum == ChecksumHelper.CreateChecksum(File.ReadAllBytes(filename)))
                        continue;

                    GetRequestEntity ent = new GetRequestEntity();
                    ent.filename = file.filename;

                    GetResponseEntity getresult = client.SendCommand<GetResponseEntity>(ent);

                    if (getresult.Status == (int)StatusCode.NotFound)
                        continue;

                    var bytes = Convert.FromBase64String(getresult.content);
                    using (var imageFile = new FileStream(filename, FileMode.Create))
                    {
                        imageFile.Write(bytes, 0, bytes.Length);
                        imageFile.Flush();
                    }
                    CreateCheckSumFile(coder.GetString(Convert.FromBase64String(file.filename)),bytes);
                }

                if (files.Any(el => el.Name == filename) == false)
                {
                    GetRequestEntity ent = new GetRequestEntity();
                    ent.filename = file.filename;

                    GetResponseEntity getresult = client.SendCommand<GetResponseEntity>(ent);

                    if (getresult.Status == (int)StatusCode.NotFound)
                        continue;

                    var bytes = Convert.FromBase64String(getresult.content);
                    using (var imageFile = new FileStream(filename, FileMode.Create))
                    {
                        imageFile.Write(bytes, 0, bytes.Length);
                        imageFile.Flush();
                    }
                    CreateCheckSumFile(coder.GetString(Convert.FromBase64String(file.filename)), bytes);
                }
            }
            //TODO: nieuwe bestanden van de client met PUT sturen.
            foreach (FileInfo fileInfo in files)
            {
                if (response.files.Any(el=> el.filename == fileInfo.Name) == false)
                {
                    SyncClient newCLient = new SyncClient(
                    IPAddress.Parse(FolderProvider.Host), FolderProvider.port);

                    Encoding coderNew = Encoding.UTF8;

                    PutResponseEntity putResponse = client.SendCommand<PutResponseEntity>(new PutRequestentity()
                    {
                        filename = Convert.ToBase64String(coder.GetBytes(fileInfo.Name)),
                        checksum = ChecksumHelper.CreateChecksum(File.ReadAllBytes(fileInfo.FullName)),
                        content = Convert.ToBase64String(File.ReadAllBytes(fileInfo.FullName)),
                        original_checksum = null
                    });

                    CreateCheckSumFile(fileInfo.Name, File.ReadAllBytes(fileInfo.FullName));
                }
            }

            //Update view
            GetSyncStatus();
            syncedFolderWatcher.EnableRaisingEvents = true;
        }

       

        private void GetSyncStatus()
        {
            directoryView.Items.Clear();

            SyncClient client = new SyncClient(
            IPAddress.Parse(FolderProvider.Host), FolderProvider.port);

            Encoding coder = Encoding.UTF8;

            ListResponseEntity response = client.SendCommand<ListResponseEntity>(new ListRequestentity());


            ListResponseEntity response2 = client.SendCommand<ListResponseEntity>(new ListRequestentity());

            consoleBox.AppendText($"{Environment.NewLine}Sending LIST command to server.{Environment.NewLine}");

            consoleBox.AppendText($"{Environment.NewLine}Found {response.files.Count} to sync." + Environment.NewLine);


            DirectoryInfo dinfo = new DirectoryInfo(FolderProvider.ClientFolder);
            FileInfo[] Files = dinfo.GetFiles("*");

            foreach (FileInfo file in Files)
            {
                if (response.files.Any(el => el.filename == file.Name) == false)
                {
                    ListViewItem item = new ListViewItem(new[] { file.Name, file.Directory.FullName + "/" + file.Name, file.Length.ToString(), ChecksumHelper.CreateChecksum(File.ReadAllBytes(file.FullName)), "Niet aanwezig op de server" });
                    item.Name = file.Name;
                    directoryView.Items.Add(item);
                }
            }

            foreach (FileEntityFileObject file in response.files)
            {
                string justFilename = coder.GetString(Convert.FromBase64String(file.filename));
                string filename = FolderProvider.ClientFolder + coder.GetString(Convert.FromBase64String(file.filename));

                if (File.Exists(filename))
                {
                    if (file.checksum == ChecksumHelper.CreateChecksum(File.ReadAllBytes(filename)))
                    {
                        foreach (ListViewItem item in directoryView.Items)
                        {
                            if (item.Name == justFilename)
                            {
                                item.SubItems[4].Text = "Bijgewerkt";
                                break;
                            }
                        }
                        continue;
                    }
                    else
                    {
                        foreach (ListViewItem item in directoryView.Items)
                        {
                            if (item.Name == justFilename)
                            {
                                item.SubItems[4].Text = "Niet bijgewerkt";
                                break;
                            }
                        }
                        continue;
                    }
                    
                }
                else
                {
                    ListViewItem item = new ListViewItem(new[] { justFilename, "", "", file.checksum,"Lokaal niet aanwezig"});
                    item.Name = justFilename;
                    directoryView.Items.Add(item);
                    continue;
                }

            }

        }
        private void AddFile(string name)
        {
            SyncClient client = new SyncClient(
            IPAddress.Parse(FolderProvider.Host), FolderProvider.port);

            Encoding coder = Encoding.UTF8;

            string filelocation = FolderProvider.ClientFolder + name;
            Thread.Sleep(5000);
            PutResponseEntity response = client.SendCommand<PutResponseEntity>(new PutRequestentity()
            {
                filename = Convert.ToBase64String(coder.GetBytes(name)),
                checksum = ChecksumHelper.CreateChecksum(File.ReadAllBytes(filelocation)),
                content = Convert.ToBase64String(File.ReadAllBytes(filelocation)),
                original_checksum = null
            });

            CreateCheckSumFile(name,File.ReadAllBytes(filelocation));

        }

        private static void CreateCheckSumFile(string fileName,byte[] bytesToWrite)
        {

            File.WriteAllText(FolderProvider.ChecksumFolder + fileName + ".txt",ChecksumHelper.CreateChecksum(bytesToWrite));
        }

        private async Task DeleteFile(string filepath)
        {
          string filelocation = FolderProvider.ClientFolder + filepath;

            if (File.Exists(filelocation) == false)
                return;

                SyncClient client = new SyncClient(
              IPAddress.Parse(FolderProvider.Host), FolderProvider.port);

            
            Encoding coder = Encoding.UTF8;


            var request = new DeleteRequestentity()
            {
                filename = Convert.ToBase64String(coder.GetBytes(filepath))
            };

  

            using (FileStream stream = File.OpenRead(filelocation))
            {

                request.checksum = ChecksumHelper.CreateChecksum(File.ReadAllBytes(filelocation));

            }

            DeleteResponseEntity response = client.SendCommand<DeleteResponseEntity>(request);

            if (response.Status == (int) StatusCode.NotFound)
            {
                consoleBox.AppendText($"File {filepath} not found on server. Only deleted locally.");
                return;
            }


            if (response.Status == (int) StatusCode.Conflict)
            {
                //TODO: actual prevent deleting locally.
                consoleBox.AppendText($"File {filepath} is different than the one on the server. TODO: prevent local delete somehow.");
                return;
            }
            
            File.Delete(filelocation);
            File.Delete(FolderProvider.ChecksumFolder + filepath + ".txt");

            consoleBox.AppendText($"{Environment.NewLine}{filepath} delete from server.{Environment.NewLine}");
        }

        private async void directoryView_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            var tasks = DeleteFile(directoryView.FocusedItem.Name.ToString());
            await tasks;

            directoryView.Items.RemoveByKey(directoryView.FocusedItem.Name.ToString());
        }

        private void listSync_Click(object sender, EventArgs e)
        {
            GetSyncStatus();
        }
    }
}
