﻿using System;
using System.Configuration;

namespace Protocol.Client.GUI.Providers
{
    public class FolderProvider
    {
        public static string ClientFolder => ConfigurationManager.AppSettings["LocalDirectory"];
        public static string Host => ConfigurationManager.AppSettings["hostip"];
        public static int port => Convert.ToInt32(ConfigurationManager.AppSettings["hostport"]);

        public static string ChecksumFolder => ClientFolder  +"Checksums/";

    }
}
