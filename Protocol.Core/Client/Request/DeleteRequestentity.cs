﻿using System.Text;
using Protocol.Core.Client.Response;
using Protocol.Core.Interfaces;

namespace Protocol.Core.Client.Request
{
    public class DeleteRequestentity : EntitieBase
    {

        public DeleteRequestentity()
        {
            Command = "DELETE";
        }

        public string filename { get; set; }

        public string checksum { get; set; }
    }
}
