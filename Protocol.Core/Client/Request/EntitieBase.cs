﻿using System.Text;
using Newtonsoft.Json;
using Protocol.Core.Interfaces;

namespace Protocol.Core.Client.Request
{
    public class EntitieBase: IProtocolRequestCommand
    {

        public EntitieBase()
        {
            ProtocolVersion = "idh14sync/1.0";
        }

        public  override string ToString()
        {
            StringBuilder message = new StringBuilder();
            message.AppendLine(Command +" "+ ProtocolVersion);

            message.AppendLine("");
            message.AppendLine("");

            message.Append(JsonConvert.SerializeObject(this));

            return message.ToString();
        }

        public string Command { get; set; }
        public string ProtocolVersion { get; set; }
    }
}
