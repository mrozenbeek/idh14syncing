﻿using Protocol.Core.Client.Response;

namespace Protocol.Core.Client.Request
{
    public class GetRequestEntity:EntitieBase
    {
        public GetRequestEntity()
        {
            Command = "GET";
        }
        public string filename { get; set; }
    }
}
