﻿using System.Text;
using Protocol.Core.Client.Response;

namespace Protocol.Core.Client.Request
{
    public class ListRequestentity : EntitieBase
    {
        public ListRequestentity()
        {
            Command = "LIST";
        }

        public override string ToString()
        {
            StringBuilder message = new StringBuilder();
            message.AppendLine(Command + " " + ProtocolVersion);

            message.AppendLine("");
            message.AppendLine("");

            message.AppendLine("{}");
            return message.ToString();
        }
    }
}
