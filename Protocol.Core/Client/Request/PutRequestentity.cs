﻿using System.Text;
using Protocol.Core.Client.Response;
using Protocol.Core.Interfaces;

namespace Protocol.Core.Client.Request
{
    public class PutRequestentity : EntitieBase
    {

        public PutRequestentity()
        {
            Command = "PUT";
        }

        public string filename { get; set; }

        public string checksum { get; set; }

        public string original_checksum { get; set; }

        public string content { get; set; }
    }
}
