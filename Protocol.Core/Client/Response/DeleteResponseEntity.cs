﻿using Protocol.Core.Interfaces;

namespace Protocol.Core.Client.Response
{
    public class DeleteResponseEntity: IProtocolResponse
    {
        public string Message { get; set; }
        public int Status { get; set; }
    }
}
