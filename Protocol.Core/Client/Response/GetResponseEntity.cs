﻿using Protocol.Core.Interfaces;

namespace Protocol.Core.Client.Response
{
    public class GetResponseEntity: IProtocolResponse
    {
        public string filename { get; set; }
        public string checksum { get; set; }
        public string content { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }
    }
}
