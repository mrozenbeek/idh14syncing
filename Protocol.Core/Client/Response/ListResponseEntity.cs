﻿using System.Collections.Generic;
using Protocol.Core.Client.Request;
using Protocol.Core.Interfaces;

namespace Protocol.Core.Client.Response
{
    public class ListResponseEntity : IProtocolResponse
    {
        public ListResponseEntity()
        { 
            files = new List<FileEntityFileObject>();

        }
        public List<FileEntityFileObject> files { get; set; }
        public int Status { get; set; }
        public string Message { get; set; }
    }
}
