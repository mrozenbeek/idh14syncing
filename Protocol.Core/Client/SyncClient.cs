﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Newtonsoft.Json;
using Protocol.Core.Client.Request;
using Protocol.Core.Client.Response;
using Protocol.Core.Interfaces;

namespace Protocol.Core.Client
{
    public class SyncClient
    {
        private readonly IPAddress _ipAddress;
        private readonly int _port;
        private readonly Encoding _messageEncoding;

        public SyncClient(IPAddress ipAddress, int port)
        {
            _ipAddress = ipAddress;
            _port = port;
            _messageEncoding = Encoding.GetEncoding("UTF-8");
        }

        private string ExecuteRequest(IProtocolRequestCommand command)
        {

            StringBuilder sb = new StringBuilder();

            TcpClient client = new TcpClient();

            try
            {
                client.Connect(_ipAddress, _port);

                using (NetworkStream stream = client.GetStream())
                {
                    // Write the request to the stream
                    byte[] bytes = CreateRequestMessage(command);
                    stream.Write(bytes, 0, bytes.Length);


                    var decoder = Encoding.UTF8.GetDecoder();

                    int inputByteCount;
                    byte[] inputBuffer = new byte[8192];
                    char[] charBuffer = new char[Encoding.UTF8.GetMaxCharCount(inputBuffer.Length)];

                    while ((inputByteCount = stream.Read(inputBuffer, 0, inputBuffer.Length)) > 0)
                    {
                        int readChars = decoder.GetChars(inputBuffer, 0, inputByteCount, charBuffer, 0);
                        if (readChars > 0)
                            sb.Append(charBuffer, 0, readChars);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Communication with the server failed.", ex);
            }
            finally
            {
                // ALWAYS close the connection, even when an error occured
                if (client.Connected)
                    client.Close();
            }

            return sb.ToString();
        }

        /// <summary>
        /// Creates the request message and returns it as a byte array.
        /// </summary>
        private byte[] CreateRequestMessage(IProtocolRequestCommand command)
        {
            return _messageEncoding.GetBytes(command.ToString());
        }

        /// <summary>
        /// Gets a list with the names and checksums of all the files on the server.
        /// </summary>
        public T SendCommand<T>(IProtocolRequestCommand command) where T : IProtocolResponse
        {
            string responseMessage = ExecuteRequest(command);

            string[] responseSplit = responseMessage.Split(new[] {"\r\n\r\n"}, StringSplitOptions.None);
            return JsonConvert.DeserializeObject<T>(responseSplit[1]);
        }

    }
}
