﻿namespace Protocol.Core.Enums
{
    public enum StatusCode
    {
        NotFound=404,
        Ok=200,
        Wtf=400,
        Conflict=412
    }
}
