﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Protocol.Core.Helpers
{
   public static  class ChecksumHelper
    {

        private static readonly HashAlgorithm Algorithm;

        static ChecksumHelper()
        {
            Algorithm = HashAlgorithm.Create("SHA1");
        }

        public static string CreateChecksum(byte[] bytes)
        {
            byte[] checksumBytes = Algorithm.ComputeHash(bytes);
            return CreateChecksumString(checksumBytes);
        }

        public static string CreateChecksum(Stream stream)
        {
            byte[] checksumBytes = Algorithm.ComputeHash(stream);
            return CreateChecksumString(checksumBytes);
        }

        private static string CreateChecksumString(byte[] checksumBytes)
        {
            return BitConverter.ToString(checksumBytes)
                .Replace("-", string.Empty)
                .ToLower();
        }
    }
}
