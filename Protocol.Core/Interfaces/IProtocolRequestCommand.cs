﻿using System;
using Protocol.Core.Enums;

namespace Protocol.Core.Interfaces
{
    public interface IProtocolRequestCommand
    {
        string ProtocolVersion { get; set; }


    }
}
