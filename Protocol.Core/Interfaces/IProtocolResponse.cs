﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Protocol.Core.Interfaces
{
    public interface IProtocolResponse
    {
        int Status { get; set; }
        string Message { get; set; }
    }
}
