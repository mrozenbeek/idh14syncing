﻿using System;
using Protocol.Core.Enums;

namespace Protocol.Core.Interfaces
{
    public interface IProtocolServerCommand
    {

        string Command { get; set; }
        string ProtocolVersion { get; set; }

        Boolean IsForMe(string header);

        string ProcessRequest(string body);

    }
}
