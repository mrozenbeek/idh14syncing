﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Protocol.Core.Interfaces;
using Protocol.Core.Server.Commands;

namespace Protocol.Core.Ninject
{
    public class CommandInstaller: IWindsorInstaller
    {


        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            container.Register(Classes.FromAssemblyContaining<IProtocolServerCommand>().BasedOn<IProtocolServerCommand>().WithService.FromInterface());
        }
    }
}
