﻿using System;
using Protocol.Core.Enums;
using Protocol.Core.Interfaces;

namespace Protocol.Core.Server.Commands
{
    public abstract class CommandBase<T> : IProtocolServerCommand where T : IProtocolResponse, new()
    {
        public string Command { get; set; }
        public string ProtocolVersion { get; set; }
        public T CommandObject { get; set; }
            

        protected CommandBase()
        {
            ProtocolVersion = "idh14sync/1.0";
            CommandObject = new T();
            CommandObject.Status = (int)StatusCode.Ok;
        }

        public bool IsForMe(string header)
        {
            return header.StartsWith(Command + " " + ProtocolVersion);
        }

        public abstract  string ProcessRequest(string body);


    }
}
