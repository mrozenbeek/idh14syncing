﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Protocol.Core.Client.Request;
using Protocol.Core.Client.Response;
using Protocol.Core.Enums;
using Protocol.Core.Helpers;

namespace Protocol.Core.Server.Commands
{
    public class DeleteCommand : CommandBase<DeleteResponseEntity>
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (typeof(DeleteCommand));

        public DeleteCommand()
        {
            Command = "DELETE";
        }

        public override string ProcessRequest(string body)
        {
            Encoding enc = Encoding.UTF8;

            DeleteRequestentity ent = JsonConvert.DeserializeObject<DeleteRequestentity>(body);
            string justFilename = enc.GetString(Convert.FromBase64String(ent.filename));
            string filename = FolderProvider.ClientFolder + enc.GetString(Convert.FromBase64String(ent.filename));

            log.Info($"Processing Delete command for {filename}");

            if (File.Exists(filename) == false)
            {

                CommandObject.Message = "File not Found";
                CommandObject.Status = (int) StatusCode.NotFound;
                log.Info($"{justFilename} not found while executing delete commmand. Sending {CommandObject.Message} to the client.");
            }

            if (ent.checksum == ChecksumHelper.CreateChecksum(File.ReadAllBytes(filename)))
            {
                File.Delete(filename);
                CommandObject.Status = (int) StatusCode.Ok;
                log.Info($"{justFilename} was newer on the server and has been deleted.");
            }
            else
            {
                CommandObject.Message = "You are trying to delete an older version of a document";
                CommandObject.Status = (int) StatusCode.Conflict;
                log.Info($"{justFilename}");
                log.Info($"{justFilename} was not newer on the server and has not been deleted. Sending {CommandObject.Message} to the client.");
            }
                return JsonConvert.SerializeObject(CommandObject);
        }


    }
}