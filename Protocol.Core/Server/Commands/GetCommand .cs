﻿using System;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using Protocol.Core.Client.Request;
using Protocol.Core.Client.Response;
using Protocol.Core.Enums;
using Protocol.Core.Helpers;

namespace Protocol.Core.Server.Commands
{

    public class GetCommand : CommandBase<GetResponseEntity>
    {

        public GetCommand()
        {
            Command = "GET";
        }

        public override string ProcessRequest(string body)
        {

            Encoding enc = Encoding.UTF8;
            
            GetRequestEntity ent = JsonConvert.DeserializeObject<GetRequestEntity>(body);

            string filename = enc.GetString(Convert.FromBase64String(ent.filename));

            if (File.Exists(FolderProvider.ClientFolder + filename))
            {

                    CommandObject.content =
                        Convert.ToBase64String(File.ReadAllBytes(FolderProvider.ClientFolder + filename));
                    CommandObject.checksum =
                        ChecksumHelper.CreateChecksum(File.ReadAllBytes(FolderProvider.ClientFolder + filename));

                    CommandObject.filename = ent.filename;

            }
            else
                CommandObject.Status = (int) StatusCode.NotFound;

            

            return JsonConvert.SerializeObject(CommandObject);
        }
    }
}