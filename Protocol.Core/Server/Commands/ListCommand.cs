﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Protocol.Core.Client.Request;
using Protocol.Core.Client.Response;
using Protocol.Core.Helpers;

namespace Protocol.Core.Server.Commands
{

    public class ListCommand : CommandBase<ListResponseEntity>
    {

        public ListCommand()
        {
            Command = "LIST";
        }

        public override string ProcessRequest(string body)
        {
            DirectoryInfo directory = new DirectoryInfo(FolderProvider.ClientFolder);
            FileInfo[] files = directory.GetFiles();

            var filtered = files.Where(f => !f.Attributes.HasFlag(FileAttributes.Hidden));

            Encoding encoding = Encoding.UTF8;
            
            foreach (var f in filtered)
            {
                CommandObject.files.Add(new FileEntityFileObject()
                {
                    filename = Convert.ToBase64String(encoding.GetBytes(f.Name)),
                    checksum = ChecksumHelper.CreateChecksum(File.ReadAllBytes(f.FullName))
                });
            }

            return JsonConvert.SerializeObject(CommandObject);
        }
    }
}