﻿using System;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using Protocol.Core.Client.Request;
using Protocol.Core.Client.Response;
using Protocol.Core.Enums;
using Protocol.Core.Helpers;

namespace Protocol.Core.Server.Commands
{
    public class PutCommand : CommandBase<PutResponseEntity>
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (typeof (DeleteCommand));

        public PutCommand()
        {
            Command = "PUT";
        }

        public override string ProcessRequest(string body)
        {
            Encoding enc = Encoding.UTF8;

            PutRequestentity ent = JsonConvert.DeserializeObject<PutRequestentity>(body);
            string justFilename = enc.GetString(Convert.FromBase64String(ent.filename));
            string filename = FolderProvider.ClientFolder + enc.GetString(Convert.FromBase64String(ent.filename));

            log.Info($"Processing PUT command for {filename}");

            if (File.Exists(filename) == false)
            {

                CommandObject.Message = "File added";
                CommandObject.Status = (int) StatusCode.Ok;
                log.Info($"{justFilename} added. Sending {CommandObject.Message} to the client.");
                File.WriteAllBytes(filename, Convert.FromBase64String(ent.content));
                return JsonConvert.SerializeObject(CommandObject);

            }
            else
            {
                string checksum = ChecksumHelper.CreateChecksum(File.ReadAllBytes(filename));

                if (ent.checksum == checksum)
                {
                    CommandObject.Status = (int) StatusCode.Wtf;
                    CommandObject.Message = "Wtf dude the file is the same. STOPH THAT";
                    return JsonConvert.SerializeObject(CommandObject);
                }

                if (ent.original_checksum != null && ent.original_checksum == checksum)
                {
                    CommandObject.Message = "File updated on server";
                    CommandObject.Status = (int) StatusCode.Ok;
                    log.Info($"{justFilename} updated. Sending {CommandObject.Message} to the client.");
                    File.WriteAllBytes(filename, Convert.FromBase64String(ent.content));
                    return JsonConvert.SerializeObject(CommandObject);
                }
                else
                {
                    CommandObject.Message = "File is newer on the server";
                    CommandObject.Status = (int) StatusCode.Conflict;
                    log.Info($"{justFilename} not updates. Sending {CommandObject.Message} to the client.");
                    return JsonConvert.SerializeObject(CommandObject);
                }
            }
        }
    }
}