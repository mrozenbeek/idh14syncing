﻿using System.Configuration;

namespace Protocol.Core.Server
{
    public class FolderProvider
    {
        public static string ClientFolder => ConfigurationManager.AppSettings["LocalDirectory"];
    }
}
