﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Castle.Windsor;
using Protocol.Core.Interfaces;

namespace Protocol.Core.Server
{
    public class SyncServer
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
            (typeof(SyncServer));

        private readonly int _listeningPort;
        private readonly Encoding _messageEncoding;

        private Thread _listenerThread;
        private TcpListener _listener;
        private bool _canListen;

        private static IWindsorContainer _container;


        public SyncServer(int listeningPort, IWindsorContainer container)
        {
            _listeningPort = listeningPort;
            _messageEncoding = Encoding.GetEncoding("UTF-8");
            _container = container;
        }

        public void Start()
        {

            _listenerThread = new Thread(Listen);
            _listenerThread.Start();
        }

        private void Listen()
        {
            _listener = new TcpListener(IPAddress.Any, _listeningPort);

            _listener.Start();
            _canListen = true;

            while (_canListen)
            {
                try
                {
                    TcpClient client = _listener.AcceptTcpClient();
                    Thread requestThread = new Thread(HandleRequest);
                    requestThread.Start(client);
                }
                catch (SocketException ex)
                {
                    if (ex.SocketErrorCode != SocketError.Interrupted)
                        throw;
                }
            }

        }

        private void HandleRequest(object parameter)
        {
            TcpClient client = (TcpClient)parameter;

            try
            {
                using (NetworkStream stream = client.GetStream())
                {
                    StringBuilder sb = new StringBuilder();
                    string toSend = "";



                    var decoder = Encoding.UTF8.GetDecoder();

                    int inputByteCount;
                    byte[] inputBuffer = new byte[8192];
                    char[] charBuffer = new char[Encoding.UTF8.GetMaxCharCount(inputBuffer.Length)];


                    int openingCount = 0;
                    int closingCount = 0;
                    while ((inputByteCount = stream.Read(inputBuffer, 0, inputBuffer.Length)) > 0)
                    {
                        int readChars = decoder.GetChars(inputBuffer, 0, inputByteCount, charBuffer, 0);
                        if (readChars > 0)
                        {
                            sb.Append(charBuffer, 0, readChars);

                            foreach (char character in charBuffer)
                            {
                                if (character == '{')
                                    openingCount++;
                                if (character == '}')
                                    closingCount++;
                            }

                            if (openingCount == closingCount)
                                break;

                        }

                    }

                    string[] parameters = sb.ToString().Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                    foreach (var command in _container.ResolveAll<IProtocolServerCommand>())
                    {
                        if (command.IsForMe(parameters[0]))
                        {

                            log.Info($"Command {command.Command} found in package Handling {command.Command} command now.");
                            toSend = $"RESPONSE {command.ProtocolVersion}\r\n\r\n";
                            toSend = toSend + command.ProcessRequest(parameters.Length == 1 ? "" : parameters[1]);
                            log.Info($"Command {command.Command} done.");
                        }
                    }

                    byte[] bytesToSend = Encoding.UTF8.GetBytes(toSend);
                    // Write the response message
                    stream.Write(bytesToSend, 0, bytesToSend.Length);

                }
            }
            finally
            {
                if (client.Connected)
                    client.Close();
            }
        }
    }
}
