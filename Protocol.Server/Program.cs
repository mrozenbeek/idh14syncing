﻿using System;
using Castle.Windsor;
using Protocol.Core.Ninject;
using Protocol.Core.Server;

namespace Protocol.Server
{
    class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger
        (typeof(Program));

        private static void Main(string[] args)
        {
            System.AppDomain.CurrentDomain.UnhandledException += UnhandledExceptionTrapper;
            var container = new WindsorContainer().Install(new CommandInstaller());

            log.Info("Starting...");
            SyncServer myServer = new SyncServer(8888,container);
            myServer.Start();
            log.Info("Started.");


        }

        static void UnhandledExceptionTrapper(object sender, UnhandledExceptionEventArgs e)
        {
            log.Info(e.ExceptionObject.ToString());
            log.Info("Press Enter to continue");
            Console.ReadLine();
            Environment.Exit(1);
        }


    }


}
